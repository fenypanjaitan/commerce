package com.finalproject.nexcommerce;

//import javax.annotation.Resource;

//import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//import com.finalproject.nexcommerce.services.FileStorageService;
@SpringBootApplication
public class NexcommerceApplication {
//	implements CommandLineRunner
//	@Resource
//	FileStorageService storageService;

	public static void main(String[] args) {
		SpringApplication.run(NexcommerceApplication.class, args);
	}

//	@Override
//	public void run(String... arg) throws Exception {
//		storageService.deleteAll();
//		storageService.init();
//	}
}
