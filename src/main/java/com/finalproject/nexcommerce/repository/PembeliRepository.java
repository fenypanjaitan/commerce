package com.finalproject.nexcommerce.repository;

import com.finalproject.nexcommerce.entity.Pembeli;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PembeliRepository extends JpaRepository<Pembeli, Integer> {
    Pembeli findByUsername(String username);

}
