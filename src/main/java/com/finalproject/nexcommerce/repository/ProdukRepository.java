package com.finalproject.nexcommerce.repository;

import com.finalproject.nexcommerce.entity.Produk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProdukRepository extends JpaRepository<Produk, Integer> {

    Produk findByNamaProduk(String namaProduk);

    @Query(value = "Select nama_produk from Produk p INNER JOIN Penjual j on p.id_penjual = j.nama_toko", nativeQuery = true)
    List<Produk> findProdukByIdPenjual(int idPenjual);
//    @Query("Select new com.finalproject.nexcommerce.dto.ProdukResponse(p.namaProduk, p.merek,p.harga,k.namaKategori,j.namaToko from Produk p join p.idKategori k join p.idPenjual j )")
//    List<ProdukResponse> getJoinInformation();
    @Query(value = "Select p.nama_produk, p.merek, p.harga, k.nama_kategori, j.nama_toko from Produk p join Kategori k on p.id_kategori=k.id join Penjual j on p.id_penjual=j.id", nativeQuery = true)
    List<Produk> getAllInformasi();

    @Query(value = "select * from Produk p where p.nama_produk like '%:textNamaProduk%' or p.merek like '%:textMerek%'", nativeQuery = true)
    List<Produk> findByNamaPodukContainingOrMerekContaining(String textNamaPoduk,String textMerek);

}
