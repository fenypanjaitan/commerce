package com.finalproject.nexcommerce.repository;

import com.finalproject.nexcommerce.entity.Pesanan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PesananRepository extends JpaRepository<Pesanan, Integer> {
}
