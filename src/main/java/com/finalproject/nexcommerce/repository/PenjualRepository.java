package com.finalproject.nexcommerce.repository;

import com.finalproject.nexcommerce.entity.Penjual;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PenjualRepository extends JpaRepository<Penjual, Integer> {
    Penjual findByUsername(String username);
    Penjual findByNamaToko(String namaToko);
    @Query(value = " select * from penjual j where j.status like '%:textStatus%'",nativeQuery = true)
    List<Penjual> findByStatus(String textStatus);
//    Penjual findByStatus(String status);
}
