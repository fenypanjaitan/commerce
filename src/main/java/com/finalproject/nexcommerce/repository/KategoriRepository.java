package com.finalproject.nexcommerce.repository;

import com.finalproject.nexcommerce.entity.Kategori;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KategoriRepository extends JpaRepository<Kategori, Integer> {
    Kategori findByNamaKategori(String namaKategori);
}
