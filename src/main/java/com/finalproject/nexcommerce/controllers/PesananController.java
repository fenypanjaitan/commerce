package com.finalproject.nexcommerce.controllers;

import com.finalproject.nexcommerce.entity.Pesanan;
import com.finalproject.nexcommerce.services.PesananService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins="*", allowedHeaders = "*")
@RestController
public class PesananController {
    @Autowired
    private PesananService pesananService;

    @PostMapping("/addPesanan")
    public Pesanan addPesanan(@RequestBody Pesanan pesanan){
        return pesananService.savePesanan(pesanan);
    }

    @GetMapping("/pesanans")
    public List<Pesanan> findAllPesanans(){
        return pesananService.getPesanans();
    }

    @GetMapping("/pesanans/{id}")
    public Pesanan findCostumerById(@PathVariable int id){
        return pesananService.getPesananById(id);
    }

    @PutMapping("/updatePesanan/{id}")
    public Pesanan updatePesanan(@RequestBody Pesanan pesanan, @PathVariable int id){
        pesanan.setId(id);
        return pesananService.updatePesanan(pesanan);
    }

    @DeleteMapping("/deletePesanan/{id}")
    public String deletePesanan(@PathVariable int id){
        return pesananService.deletePesanan(id);
    }
}
