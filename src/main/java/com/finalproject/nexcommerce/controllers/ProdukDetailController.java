package com.finalproject.nexcommerce.controllers;

import com.finalproject.nexcommerce.entity.Produk;
import com.finalproject.nexcommerce.services.ProdukDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController

public class ProdukDetailController {
    @Autowired
    private ProdukDetailService pdService;

//    @GetMapping("/produk")
//    public List<ProdukResponse> getJoinInformationn(){
//        return produkRepository.getJoinInformation();
//    }

    @GetMapping("/produkkk")
    public List<Produk> findAllInformasiProduks(){
        return pdService.getInformasiProduks();
    }
}
