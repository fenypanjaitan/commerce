package com.finalproject.nexcommerce.controllers;

import com.finalproject.nexcommerce.entity.Admin;
import com.finalproject.nexcommerce.repository.AdminRepository;
import com.finalproject.nexcommerce.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins="*", allowedHeaders = "*")
@RestController
public class AdminController {
    @Autowired
    private AdminService adminService;

    @Autowired
    private AdminRepository adminRepository;

    @PostMapping("/addAdmin")
    public Admin addAdmin(@RequestBody Admin admin){
        return adminService.saveAdmin(admin);
    }

    @GetMapping("/admins")
    public List<Admin> findAllAdmins(){
        return adminService.getAdmins();
    }

    @GetMapping("/admin/{username}")
    public Admin findByUsername(@PathVariable String username){
        return adminService.getUsername(username);
    }
}
