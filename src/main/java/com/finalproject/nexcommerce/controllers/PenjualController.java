package com.finalproject.nexcommerce.controllers;

import com.finalproject.nexcommerce.entity.Penjual;
import com.finalproject.nexcommerce.services.PenjualService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins="*", allowedHeaders = "*")
@RestController
public class PenjualController {
    @Autowired
    private PenjualService penjualService;

    @PostMapping("/addPenjual")
    public Penjual addPenjual(@RequestBody Penjual penjual){
        return penjualService.savePenjual(penjual);
    }

    @GetMapping("/penjuals")
    public List<Penjual> findAllPenjuals(){
        return penjualService.getPenjuals();
    }

    @GetMapping("/penjuals/{id}")
    public Penjual findPenjualById(@PathVariable int id){
        return penjualService.getPenjualById(id);
    }

    @GetMapping("/toko/{namaToko}")
    public Penjual findPenjualByNamaToko(@PathVariable String namaToko){
        return penjualService.getPenjualByNamaToko(namaToko);
    }

//    @GetMapping("/statusPenjual/{status}")
//    public List<Penjual> findPenjualByStatus(@PathVariable String status){
//        return penjualService.getPenjualByStatus(status);
//    }

    @ResponseStatus(value = HttpStatus.OK)
    @GetMapping(value = "/statusPenjual/{status}")
    public List<Penjual> process(@PathVariable String status) {
        return penjualService.getPenjualByStatus(status);
    }
//    @GetMapping("/statusPenjual/{status}")
//    public Penjual findPenjualByStatus(@PathVariable String status){
//        return penjualService.getPenjualByStatus(status);
//    }

    @PutMapping("/updatePenjual/{id}")
    public Penjual updatePenjual(@RequestBody Penjual penjual, @PathVariable int id){
        penjual.setId(id);
        return penjualService.updatePenjual(penjual);
    }

    @DeleteMapping("/deletePenjual/{id}")
    public String deletePenjual(@PathVariable int id){
        return penjualService.deletePenjual(id);
    }

    @GetMapping("/penjual/{username}")
    public Penjual findByUsername(@PathVariable String username){
        return penjualService.getUsername(username);
    }
}
