package com.finalproject.nexcommerce.controllers;

import com.finalproject.nexcommerce.entity.Kategori;
import com.finalproject.nexcommerce.services.KategoriService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*",allowedHeaders = "")
@RestController
public class KategoriController {
    @Autowired
    private KategoriService kategoriService;

    @PostMapping("/addKategori")
    public Kategori addKategori(@RequestBody Kategori kategori){
        return kategoriService.saveKategori(kategori);
    }

    @GetMapping("/kategoris")
    public List<Kategori> findAllKategoris(){
        return kategoriService.getKategoris();
    }

    @GetMapping("/kategoris/{id}")
    public Kategori findKategoriById(@PathVariable int id){
        return kategoriService.getKategoriById(id);
    }

    @GetMapping("/kategori/{namaKategori}")
    public Kategori findKategoriNamaKategori(@PathVariable String namaKategori){
        return kategoriService.getKategoriByNamaKategori(namaKategori);
    }

    @PutMapping("/updateKategori/{id}")
    public Kategori updateKategori(@RequestBody Kategori kategori, @PathVariable int id){
        kategori.setId(id);
        return kategoriService.updateKategori(kategori);
    }

    @DeleteMapping("/deleteKategori/{id}")
    public String deleteKategori(@PathVariable int id){
        return kategoriService.deleteKategori(id);
    }
}
