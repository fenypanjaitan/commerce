package com.finalproject.nexcommerce.controllers;

import com.finalproject.nexcommerce.entity.Pembeli;
import com.finalproject.nexcommerce.services.PembeliService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin(origins="*", allowedHeaders = "*")
@RestController
public class PembeliController {
    @Autowired
    private PembeliService pembeliService;

    @PostMapping("/addPembeli")
    public Pembeli addPembeli(@RequestBody Pembeli pembeli){
        return pembeliService.savePembeli(pembeli);
    }

    @GetMapping("/pembelis")
    public List<Pembeli> findAllPembelis(){
        return pembeliService.getPembelis();
    }

    @GetMapping("/pembelis/{id}")
    public Pembeli findCostumerById(@PathVariable int id){
        return pembeliService.getPembeliById(id);
    }

    @PutMapping("/updatePembeli/{id}")
    public Pembeli updatePembeli(@RequestBody Pembeli pembeli, @PathVariable int id){
        pembeli.setId(id);
        return pembeliService.updatePembeli(pembeli);
    }

    @DeleteMapping("/deletePembeli/{id}")
    public String deletePembeli(@PathVariable int id){
        return pembeliService.deletePembeli(id);
    }

    @GetMapping("/pembeli/{username}")
    public Pembeli findByUsername(@PathVariable String username){
        return pembeliService.getUsername(username);
    }

}
