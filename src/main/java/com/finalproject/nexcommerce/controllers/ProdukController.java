package com.finalproject.nexcommerce.controllers;

import com.finalproject.nexcommerce.entity.Produk;
import com.finalproject.nexcommerce.repository.ProdukRepository;
import com.finalproject.nexcommerce.services.ProdukService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins="*", allowedHeaders = "*")
@RestController
public class ProdukController {
    @Autowired
    private ProdukService produkService;

    @PostMapping("/addProduk")
    public Produk addProduk(@RequestBody Produk produk){
        return produkService.saveProduk(produk);
    }

    @GetMapping("/produks")
    public List<Produk> findAllProduks(){
        return produkService.getProduks();
    }

    @GetMapping("/produks/{id}")
    public Produk findProdukById(@PathVariable int id){
        return produkService.getProdukById(id);
    }

    @GetMapping("/produkByIdPenjual/{idPenjual}")
    public Produk findProdukByIdPenjual(@PathVariable int idPenjual){
        return (Produk) produkService.getProdukByIdPenjual(idPenjual);
    }


    @PutMapping("/updateProduk/{id}")
    public Produk updateProduk(@RequestBody Produk produk, @PathVariable int id){
        produk.setId(id);
        return produkService.updateProduk(produk);
    }

    @DeleteMapping("/deleteProduk/{id}")
    public String deleteProduk(@PathVariable int id){
        return produkService.deleteProduk(id);
    }

    @Autowired
    ProdukRepository produkRepository;

    @PostMapping("/produk/search")
    public List<Produk> search(@RequestBody Map<String, String> body) {
        String searchTerm = body.get("text");
        return produkRepository.findByNamaPodukContainingOrMerekContaining(searchTerm, searchTerm);
    }

    @GetMapping("/produk/{namaProduk}")
    public Produk findByNamaProduk(@PathVariable String namaProduk){
        return produkService.getNamaProduk(namaProduk);
    }

}
