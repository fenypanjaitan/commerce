package com.finalproject.nexcommerce.services;

import com.finalproject.nexcommerce.entity.Pesanan;
import com.finalproject.nexcommerce.repository.PesananRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PesananService {
    @Autowired
    private PesananRepository pesananRepository;

    public Pesanan savePesanan(Pesanan pesanan){
        return pesananRepository.save(pesanan);
    }

    public List<Pesanan> getPesanans(){
        return pesananRepository.findAll();
    }

    public Pesanan getPesananById(int id){
        return pesananRepository.findById(id).orElse(null);
    }

    public String deletePesanan(int id){
        pesananRepository.deleteById(id);
        return "Pesanan dihapus";
    }

    public Pesanan updatePesanan(Pesanan pesanan){
        Pesanan existingPesanan = pesananRepository.findById(pesanan.getId()).orElse(null);
        existingPesanan.setTanggal(pesanan.getTanggal());
        existingPesanan.setStatus(pesanan.getStatus());
        existingPesanan.setKuantitas(pesanan.getKuantitas());
        existingPesanan.setHargaTotal(pesanan.getHargaTotal());
        existingPesanan.setProduk(pesanan.getProduk());

        return pesananRepository.save(existingPesanan);
    }
}
