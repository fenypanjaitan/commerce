package com.finalproject.nexcommerce.services;

import com.finalproject.nexcommerce.entity.Produk;
import com.finalproject.nexcommerce.repository.ProdukRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProdukService {
    @Autowired
    private ProdukRepository produkRepository;

    public Produk saveProduk(Produk produk){
        return produkRepository.save(produk);
    }

    public List<Produk> getProduks(){
        return produkRepository.findAll();
    }

    public Produk getProdukById(int id){
        return produkRepository.findById(id).orElse(null);
    }

//    public List<Produk> getProdukByIdPenjual(int idPenjual){
//        return produkRepository.findByIdPenjual(idPenjual);
//    }

    public String deleteProduk(int id){
        produkRepository.deleteById(id);
        return "Produk dihapus";
    }

    public Produk getProdukByIdPenjual(int idPenjual){
        return (Produk) produkRepository.findProdukByIdPenjual(idPenjual);
    }

    public Produk getNamaProduk(String namaProduk){
        return produkRepository.findByNamaProduk(namaProduk);
    }

    public Produk updateProduk(Produk produk){
        Produk existingProduk = produkRepository.findById(produk.getId()).orElse(null);
        existingProduk.setImage(produk.getImage());
        existingProduk.setNamaProduk(produk.getNamaProduk());
        existingProduk.setDeskripsi(produk.getDeskripsi());
        existingProduk.setMerek(produk.getMerek());
        existingProduk.setKadaluarsa(produk.getKadaluarsa());
        existingProduk.setHarga(produk.getHarga());
        existingProduk.setStok(produk.getStok());
        existingProduk.setVariasi(produk.getVariasi());
        existingProduk.setBerat(produk.getBerat());
        existingProduk.setKategori(produk.getKategori());

        return produkRepository.save(existingProduk);
    }

}
