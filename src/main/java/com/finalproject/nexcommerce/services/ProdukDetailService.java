package com.finalproject.nexcommerce.services;

import com.finalproject.nexcommerce.entity.Produk;
import com.finalproject.nexcommerce.repository.ProdukRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProdukDetailService {
    @Autowired
    private ProdukRepository produkRepository;

    public List<Produk> getInformasiProduks(){
        return produkRepository.getAllInformasi();
    }


}
