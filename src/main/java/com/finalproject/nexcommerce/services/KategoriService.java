package com.finalproject.nexcommerce.services;

import com.finalproject.nexcommerce.entity.Kategori;
import com.finalproject.nexcommerce.repository.KategoriRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KategoriService {
    @Autowired
    private KategoriRepository kategoriRepository;


    public Kategori saveKategori(Kategori kategori){
        return kategoriRepository.save(kategori);
    }

    public List<Kategori> getKategoris(){
        return kategoriRepository.findAll();
    }

    public Kategori getKategoriById(int id){
        return kategoriRepository.findById(id).orElse(null);
    }

    public Kategori getKategoriByNamaKategori(String namaKategori) {
        return kategoriRepository.findByNamaKategori(namaKategori);
    }

    public String deleteKategori(int id){
        kategoriRepository.deleteById(id);
        return "Kategori dihapus";
    }

    public Kategori updateKategori(Kategori kategori){
        Kategori existingKategori = kategoriRepository.findById(kategori.getId()).orElse(null);
        existingKategori.setNamaKategori(kategori.getNamaKategori());

        return kategoriRepository.save(existingKategori);
    }

}
