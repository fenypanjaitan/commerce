package com.finalproject.nexcommerce.services;

import com.finalproject.nexcommerce.entity.Penjual;
import com.finalproject.nexcommerce.repository.PenjualRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PenjualService {
    @Autowired
    private PenjualRepository penjualRepository;

    public Penjual savePenjual(Penjual penjual){
        return penjualRepository.save(penjual);
    }

    public List<Penjual> getPenjuals(){
        return penjualRepository.findAll();
    }

    public Penjual getPenjualById(int id){
        return penjualRepository.findById(id).orElse(null);
    }

    public Penjual getUsername(String username){
        return penjualRepository.findByUsername(username);
    }

    public Penjual getPenjualByNamaToko(String namaToko) {
        return penjualRepository.findByNamaToko(namaToko);
    }

    public List<Penjual> getPenjualByStatus(String status) {
        return penjualRepository.findByStatus(status);
    }
//    public Penjual getPenjualByStatus(String status) {
//        return penjualRepository.findByStatus(status);
//    }

    public String deletePenjual(int id){
        penjualRepository.deleteById(id);
        return "Penjual dihapus";
    }

    public Penjual updatePenjual(Penjual penjual){
        Penjual existingPenjual = penjualRepository.findById(penjual.getId()).orElse(null);
        existingPenjual.setUsername(penjual.getUsername());
        existingPenjual.setPassword(penjual.getPassword());
        existingPenjual.setNamaToko(penjual.getNamaToko());
        existingPenjual.setAlamat(penjual.getAlamat());
        existingPenjual.setNoTelp(penjual.getNoTelp());
        existingPenjual.setStatus(penjual.getStatus());
        existingPenjual.setEmail(penjual.getEmail());

        return penjualRepository.save(existingPenjual);
    }


}
