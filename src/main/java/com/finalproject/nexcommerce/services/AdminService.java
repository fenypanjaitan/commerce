package com.finalproject.nexcommerce.services;

import com.finalproject.nexcommerce.entity.Admin;
import com.finalproject.nexcommerce.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {
    @Autowired
    private AdminRepository adminRepository;

    public Admin saveAdmin(Admin admin){
        return adminRepository.save(admin);
    }

    public List<Admin> getAdmins(){
        return adminRepository.findAll();
    }

    public Admin getUsername(String username){
        return adminRepository.findByUsername(username);
    }
}
