package com.finalproject.nexcommerce.services;

import com.finalproject.nexcommerce.entity.Pembeli;
import com.finalproject.nexcommerce.repository.PembeliRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PembeliService {
    @Autowired
    private PembeliRepository pembeliRepository;

    public Pembeli savePembeli(Pembeli pembeli){
        return pembeliRepository.save(pembeli);
    }

    public List<Pembeli> getPembelis(){
        return pembeliRepository.findAll();
    }

    public Pembeli getPembeliById(int id){
        return pembeliRepository.findById(id).orElse(null);
    }

    public String deletePembeli(int id){
        pembeliRepository.deleteById(id);
        return "pembeli dihapus";
    }

    public Pembeli updatePembeli(Pembeli pembeli){
        Pembeli existingPembeli = pembeliRepository.findById(pembeli.getId()).orElse(null);
        existingPembeli.setNama(pembeli.getNama());
        existingPembeli.setUsername(pembeli.getUsername());
        existingPembeli.setPassword(pembeli.getPassword());
        existingPembeli.setAlamat(pembeli.getAlamat());
        existingPembeli.setNoTelp(pembeli.getNoTelp());

        return pembeliRepository.save(existingPembeli);
    }

    public Pembeli getUsername(String username){
        return pembeliRepository.findByUsername(username);
    }
}
