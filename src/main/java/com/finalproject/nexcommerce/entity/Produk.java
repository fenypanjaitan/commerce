package com.finalproject.nexcommerce.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "produk")
public class Produk {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 4)
    private int id;

    @Column(nullable = true)
    private String image;

    @Column(nullable = false, length = 30)
    private String namaProduk;

    @Column(nullable = false, length = 100)
    private String deskripsi;

    @Column(nullable = false, length = 20)
    private String merek;

    @Column(nullable = false, length = 30)
    private String kadaluarsa;

    @Column(nullable = false, length = 11)
    private int harga;

    @Column(nullable = false, length = 4)
    private int stok;

    @Column(nullable = false, length = 100)
    private String variasi;

    @Column(nullable = false, length = 4)
    private int berat;

    @Column(nullable = true, length = 4)
    private int penilaian;

    @ManyToOne()
    @JoinColumn(name = "idPenjual", nullable = false)
    private Penjual penjual;

    @OneToOne()
    @JoinColumn(name = "idKategori", nullable = false)
    private Kategori kategori;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNamaProduk() {
        return namaProduk;
    }

    public void setNamaProduk(String namaProduk) {
        this.namaProduk = namaProduk;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getMerek() {
        return merek;
    }

    public void setMerek(String merek) {
        this.merek = merek;
    }

    public String getKadaluarsa() {
        return kadaluarsa;
    }

    public void setKadaluarsa(String kadaluarsa) {
        this.kadaluarsa = kadaluarsa;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public int getStok() {
        return stok;
    }

    public void setStok(int stok) {
        this.stok = stok;
    }

    public String getVariasi() {
        return variasi;
    }

    public void setVariasi(String variasi) {
        this.variasi = variasi;
    }

    public int getBerat() {
        return berat;
    }

    public void setBerat(int berat) {
        this.berat = berat;
    }

    public int getPenilaian() {
        return penilaian;
    }

    public void setPenilaian(int penilaian) {
        this.penilaian = penilaian;
    }

    public Penjual getPenjual() {
        return penjual;
    }

    public void setPenjual(Penjual penjual) {
        this.penjual = penjual;
    }

    public Kategori getKategori() {
        return kategori;
    }

    public void setKategori(Kategori kategori) {
        this.kategori = kategori;
    }
}
